ABOUT THIS MODULE
-----------------

This module is used to convert your existing Drupal blog into the WP Blog
module by re-assigning the content type for existing nodes and moving any
existing tags into the WP Blog taxonomy vocabulary.

USAGE
-----

* Enable the 'WP Blog' and 'WP Blog Migrate' modules.
* Go to admin/config/content/wp-blog-migrate
* Define which content type currently holds your blog posts.
* Define which fields are currently used to store your tags.
* If you want WP Blog Migrate to attempt to delete your original content type,
  check the box.
* Click 'Go!'.
* Done.

AUTHOR
------

Oliver Davies - http://drupal.org/user/381388